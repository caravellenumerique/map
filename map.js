var marker;

$( document ).ready(function() {
    // mapbox
    mapboxgl.accessToken = 'pk.eyJ1Ijoia2VvbWEiLCJhIjoiY2trNzQ1MDQzMDk1OTJwbnZmcnZ4eG03aCJ9.WvtAeRWaDq06UQEd_BFyRg';
    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/keoma/ckk74rfnk01yk17o1gds7vz0m',
      center: [2.4215, 48.9083], // starting position [lng, lat]
      zoom: 10, // starting zoom
      pitch: 40, // pitch in degrees
    });

    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl({showCompass: true, visualizePitch: true}));

    // create a HTML element for each feature
    var el = document.createElement('div');
    el.className = 'marker';

    marker = new mapboxgl.Marker(el)
    .setLngLat([2.4215, 48.9083])
    .addTo(map);

    // update marker position
    map.on('load', function() {
        // load data
        updateCaravelle();
        setInterval(function () {
            updateCaravelle();
        }, 10000);
    });
});

function updateCaravelle(){
    $.getJSON("https://api.thingspeak.com/channels/1295900/feeds.json?results=2", function(data) {
        if (data.feeds.length > 0){
            console.log(data.feeds[0]);
            marker.setLngLat([data.feeds[0].field2, data.feeds[0].field1]);
        }
    });
}
